# SEOB Hacked - README #

The aim of this repository is to store the data necessary to replicate fig 5 of [1] and, most 
importantly, provide a quick guide to extracting the underlying dynamics (and more!) of the SEOB approximant family, 
in particular [2], as coded in the publicly available LALSuite library [3]
  
### Code version ###

[![C version](https://img.shields.io/badge/C_release-v1.0-green.svg)](https://bitbucket.org/RoxGamba/seobnrv4_hacked_ej/src/master/) 
Current version

### Description ###

The repository contains two main folders:

* data: this folder contains four more subfolders, dedicated to storing the waveforms, the full dynamics, the E(j) data files used in [1]
  and finally the E(J) matlab figures.

* lal_instructions: here you will find a readme file, containing the instructions you have to follow to get started with LAL, and a .c
  file necessary to obtain the ful dynamics

### Credits ###
If you used the data contained in this repository or followed the short guide provided, please do consider citing:

[1] [Nagar et al, 2019](http://inspirehep.net/record/1730540)
```
@article{Nagar:2019wds,
      author         = "Nagar, Alessandro and Pratten, Geraint and
                        Riemenschneider, Gunnar and Gamba, Rossella",
      title          = "{A Multipolar Effective One Body Model for Non-Spinning
                        Black Hole Binaries}",
      year           = "2019",
      eprint         = "1904.09550",
      archivePrefix  = "arXiv",
      primaryClass   = "gr-qc",
      SLACcitation   = "%%CITATION = ARXIV:1904.09550;%%"
}
```

[2] [Bohe et al, 2016](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.95.044028)
```
@article{Bohe:2016gbl,
      author         = "Boh�, Alejandro and others",
      title          = "{Improved effective-one-body model of spinning,
                        nonprecessing binary black holes for the era of
                        gravitational-wave astrophysics with advanced detectors}",
      journal        = "Phys. Rev.",
      volume         = "D95",
      year           = "2017",
      number         = "4",
      pages          = "044028",
      doi            = "10.1103/PhysRevD.95.044028",
      eprint         = "1611.03703",
      archivePrefix  = "arXiv",
      primaryClass   = "gr-qc",
      reportNumber   = "LIGO-P1600315",
      SLACcitation   = "%%CITATION = ARXIV:1611.03703;%%"
}
```
[3] [LALsuite](https://lscsoft.docs.ligo.org/lalsuite/)
```
 @misc{lalsuite,
       author         = "{LIGO Scientific Collaboration}",
       title          = "{LIGO} {A}lgorithm {L}ibrary - {LALS}uite",
       howpublished   = "free software (GPL)",
       doi            = "10.7935/GT1W-FZ16",
       year           = "2018"
 }
```

### Contacts ###

* rossella.gamba@edu.unito.it
* alex@nagarsoft.com