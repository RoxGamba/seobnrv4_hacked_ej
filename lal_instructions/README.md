# LAL instructions #

## 1. Quick-start guide ##

### 1.1 Waveform generation ###

If you are only interested in generating waveforms, and **not** in obtaining the underlying dynamics of the coalescence, you should:

* Install LALSuite by typing `pip install lalsuite` on your command line

* run on you terminal `lalsim-inspiral [options]`

A list of options is given at [inspiral.c](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/inspiral_8c_source.html), and can also be 
obtained by typing `lalsim-inspiral --help`.

For example, to generate the (q, chi_1, chi_2) = (1, 0, 0) .txt waveform present is this directory, we used:

``` lalsim-inspiral --m1=10 --m2=10 --approximant=SEOBNRv4 --f-min=15 >>BBH_SEOBNRv4_1_0_0.txt ```

### 1.2 Waveform + Dynamics ###

if you're interested in knowing the dynamics (i.e: the set (t, r, phi, pr*, pphi, omega, H) ) you have to:

* Clone LALsuite from https://git.ligo.org/lscsoft/lalsuite (**note**: If you are cloning anonymously then you must use the https URL! Also, 
  please ensure that you have configured `git-lfs`)

* move inside the cloned lalsuite directory to the lalsimulation src directory: `cd lalsuite/lalsimulation/src/`

* Replace the file LALSimIMRSpinAlignedEOB.c with the one present in this repository

* move back to your lalsuite folder

* Install LALsuite. 
  This time, unfortunately, you can't simply "pip" it. Here is the [offcial installation guide](https://wiki.ligo.org/Computing/DASWG/LALSuiteInstall).
  In short, you have to type:
      * `./00boot`
      * `./configure --prefix=/choose/your/installation/path/ --enable-mpi --enable-swig-python`
      * `make`
      * `make install`
  Note that it might take a while for everithing to finish!
  
Now, to generate a waveform you simply have to *source* your installation:

`source /your/installation/path/etc/lalsuiterc` 

and run `lalsim-inspiral [options]` (see section 1.1)

## 2. Modifications to source code ##

To obtain the complete dynamics, In [LALSimIMRSpinAlignedEOB.c](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_i_m_r_spin_aligned_e_o_b_8c_source.html)
we simply added the following code (lines 2962 to 2973):


```ruby  
  /* OUTPUT FULL DYNAMICS */
  FILE *out = fopen ("FullDynamics.dat", "w");
  for (i = 0; i < (INT4) hiSRndx; i++){
    fprintf (out, "%.16e %.16e %.16e %.16e %.16e %.16e %.16e\n", dynamics->data[i],
             rVec.data[i], phiVec.data[i], prVec.data[i], pPhiVec.data[i],omegaVec->data[i], hamV->data[i]);
  }
  for (i=0; i< (INT4) rHi.length; i++){
    fprintf (out, "%.16e %.16e %.16e %.16e %.16e %.16e %.16e\n", timeHi.data[i] + tstartHi,
             rHi.data[i], phiHi.data[i], prHi.data[i], pPhiHi.data[i],omegaHi->data[i], hamVHi->data[i]);
    
  }
  fclose(out);
```

To obtain even *more* information than this (e.g: the NQC coeffcients or the waveform modes h_lm) you might consider either switching `debugOutput` 
to 1 in line 60 or appropriately deleting the `#if debugOutput`/`#endif`s where they are called inside the function.