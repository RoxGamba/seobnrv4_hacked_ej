# Important Notes #

* All files use the naming notation "BBH_SEOBNRv4_q_chi1_chi2".

* To generate the waveforms we followed the convention m1 > m2 and fixed m2 = 10. Therefore, m1 = q*10.

* The columns in the dynamics files represent, in order: t, r, phi, pr*, pphi, omega, E. All quantities are in natural units.

* The waveform file contains time in *seconds*. To directly compare with the dynamics the time array has to be: 
      * transformed to natural units (use `GMsunbyc3 = 4.925490947e-6`);
      * shifted so that the starting time is zero (instead of a negative value)

* The merger values of E and J are computed by finding the time `t_mrg_wf` corresponding to the maximum of the waveform amplitude, and 
  evaluating E and J at the value `t_mrg_dyn` closest to `t_mrg_wf`

* The 'figs' folder contains the E(J) curves + merger in matlab figure format. To open them, use e.g 
  `openfig('BBH_SEOBNRv4_1_0_0.fig', 'visible')`